# Comparison

Improved performance by **700% (5 FPS to 40 FPS)** by applying Three.js best practices, reducing segments, using perlin3D instead of perlin4D, and tweaking shader uniforms.

## Before (5 FPS)

![5 fps](images/before.jpg)

## After (40 FPS)

![40 fps](images/after.jpg)
